# Simple implementation of Reinforcement Learning (A3C) using Pytorch

Based on...

To be completed soon...

## What are the main focuses in this implementation?

* Pytorch + multiprocessing (NOT threading) for parallel training
* Discrete action environment

## Codes & Results

* [discrete_A3C_leg.py](src/discrete_A3C_leg.py): main script which invokes learning
* [networks.py](src/networks.py): file with network model (model has fixed 3 layers with total number of neurons 
  controlled by a parameter, dropout controlled by prameter, RELU activation function, entropy regularization controled 
  by a parameter)
* [workers.py](src/workers.py): module with function run that does the training; it used to contain two classes- Worker, LegWorker- but were removed 
* [shared_adam.py](src/shared_adam.py): optimizer that shares its parameters in parallel
* [utils.py](src/utils.py): useful function that can be used more than once
 
Hydra saves information about runs into outputs folder.

### Results
All plots are saved into plots folder, in a subfolder named after envoronment and then after size of the network.

Files are saved with a bunch of training parameters.

### Workers

Worker classes takes a lot of arguments, that are stored in the class as parameters and then reuses them in `run` method. 
Having some of the parameters saved into class parameter (e.g. mp.Queues) may cause some issues with "pickling objects"
Instead of having this class, same results could be achieved with having only a run method and no class- this solves the 
issue and does not cause "pickling issue".

## Using Hydra to manipulate parameters

### General description
Hydra allows to have the parameters to be stored in a config file in a "yaml" format 
(it is located in "./src/config/config.yaml"). 

### Configuration sections
The configuration is split into sections, starting with defaults (at the top of the file), followed by:
- paths: used for all paths used in the script
- legcfg: used for groupping options for leg environment
- netcfg: used for groupping network parameters
- traincfg: used for all parameters that affect training procedure

### Parameters usage
When the main script is run (e.g. `python src/discreteA3C_leg.py`), all confugration options are loaded from the config file. There is a possibility to change the default value to any desired value, simply by adding using syntax 
"section-dot-parameter", e.g. to change learning rate the command has to change to 

```python src/discreteA3C_leg.py traincfg.lr=0.001```

It is possible to change multiple parameters at once, e.g.:

```python src/discreteA3C_leg.py traincfg.lr=0.001 traincfg.chosen_cpu_n=2```

### Parameter sweeping

There is also an option to do "parameter sweeping", that is to run the script multiple times, each time with different
value of parameter. To do this, one have to use syntax as above, where the values of parameter are seprated by a coma 
and followed by "-m" at the end. For example, to try different learning rates:

```python src/discreteA3C_leg.py traincfg.lr=0.001,0.0002 traincfg.chosen_cpu_n=2 traincfg.ent_reg=0,0.15 -m```

Again, multiple parameters could be changed and/or sweped and the above example will run the script 4 times, each time 
with different combination of learning rate and entropy regularization (and a single version of 'chosen_cpu' set to 2 in
every case).

### More information
More about overriding parameters with Hydra package can be found [here](https://hydra.cc/docs/advanced/override_grammar/basic/#basic-override-syntax)

## Dependencies

Non-exhaustive list of dependencies:
- pytorch v1.13
- hydra
- numpy
- matplotlib
- leg env

### Pytorch v1.13

Previous version of pytorhc is used because never version has issues with Shared Adam module.


## TODO
- [x] (2024-04-07) Add hydra output to ignore file
- [x] (2024-04-07) Change steps to episodes in plot
- [x] (2024-04-08) Run new code with 1 worker and N code with 1 worker and find the reason for the difference in time execution- where does it lie- in network computation, or in policy or where.
- [x] (2024-04-08) Add entropy regulatisation to loss function
- [x] (2024-04-22) Add drouput regularisation
- [x] (2024-04-08) Change value that is plotted (it should not be the averaged reward)
- [x] (2024-04-10) Reorganise how files should be saved with hydra
- [x] (2024-04-15) Create a pull request for a startup script
- [x] (2024-04-18) Add all features from N-repo
- [x] (2024-04-22) Add run function based on LegWorker

### Done
