import logging


# def set_logging(do_logging):
#     if do_logging:
#         logging.basicConfig(
#             level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s"
#         )  #
#     else:
#         logging.basicConfig(
#             level=logging.CRITICAL
#         )  # Set to CRITICAL to suppress all logging except critical errors #  n


# executed in a process that performs logging
def logger_process(queue, logger_name="app"):
    # create a logger
    logger = logging.getLogger(logger_name)

    # configure a stream handler
    logger.addHandler(logging.StreamHandler())
    # log all messages, debug and up
    logger.setLevel(logging.INFO)
    # run forever
    while True:
        # consume a log message, block until one arrives
        message = queue.get()
        # check for shutdown
        if message is None:
            break
        # log the message
        logger.handle(message)
