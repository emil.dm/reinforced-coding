import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import logging

from utils import set_init


class Net(nn.Module):
    def __init__(
        self,
        state_dim,
        action_dim,
        inner_size=128,
        action_dims=6,
        total_actions=6,
        ent_reg=0.001,
        temperature=1,
        dropout_rate=0.2,
    ):
        super(Net, self).__init__()
        self.state_dim = state_dim
        self.action_dim = action_dim

        # Actor
        self.pi1 = nn.Linear(state_dim, inner_size)
        self.p_dropout1 = nn.Dropout(dropout_rate)
        self.pi2 = nn.Linear(inner_size, inner_size)
        self.p_dropout2 = nn.Dropout(dropout_rate)
        self.pi3 = nn.Linear(inner_size, action_dim)

        # Critic
        self.v1 = nn.Linear(state_dim, inner_size)
        self.v_dropout1 = nn.Dropout(dropout_rate)
        self.v2 = nn.Linear(inner_size, inner_size)
        self.v_dropout2 = nn.Dropout(dropout_rate)
        self.v3 = nn.Linear(inner_size, 1)

        set_init([self.pi1, self.pi2, self.pi3, self.v1, self.v2, self.v3])

        self.distribution = torch.distributions.Categorical

        self.T = temperature

        self.action_dims = action_dims
        self.total_actions = total_actions

        self.ent_reg = ent_reg

    def forward(self, x):
        # Actor forward pass
        # pi1 = torch.tanh(self.pi1(x))
        # pi2 = torch.tanh(self.pi2(pi1))
        pi = self.p_dropout1(torch.relu(self.pi1(x)))
        pi = self.p_dropout2(torch.relu(self.pi2(pi)))
        logits = self.pi3(pi)

        # Critic forward pass
        # v1 = torch.tanh(self.v1(x))
        # v2 = torch.tanh(self.v2(v1))
        v = self.v_dropout1(torch.relu(self.v1(x)))
        v = self.v_dropout2(torch.relu(self.v2(v)))
        values = self.v3(v)

        return logits, values

    def choose_action(self, s):
        self.eval()
        logits, _ = self.forward(s)
        logging.debug("logits.shape: ", logits.shape)

        prob = F.softmax(logits, dim=1).data
        logging.debug("prob.shape: ", prob.shape)
        m = self.distribution(prob)

        return m.sample().numpy()[0]

    # TODO max temp should be a self parameter
    def adjust_temperature(self, episode_num, max_temp=1.1, decay_rate=0.99):
        self.T = 1 + (max_temp - 1) * decay_rate**episode_num

    def get_logits_view(self, starting_logits):
        logits = starting_logits.view(
            -1, self.action_dims, self.total_actions
        )  # Reshape logits to (Batch Size, 6 dimensions, 6 values)
        return logits

    # def choose_action2(self, s, T, epi_count, t):
    def choose_action2(
        self,
        s,
    ):
        self.eval()
        starting_logits, _ = self.forward(s)
        logits = self.get_logits_view(starting_logits)

        actions = np.zeros(
            (logits.shape[0], self.action_dims), dtype=int
        )  # Initialize action array

        for dim in range(self.action_dims):  # Iterate over each dimension
            # Compute probabilities for each dimension
            probs = F.softmax(logits[:, dim, :] / self.T, dim=1).data
            m = self.distribution(probs)  # Create a distribution for each dimension
            actions[:, dim] = m.sample().numpy()  # Sample an action for each dimension

        return actions

    def loss_func(self, s, a, v_t):
        self.train()
        starting_logits, values = self.forward(s)
        logits = self.get_logits_view(starting_logits)

        temporal_diff_error = v_t - values
        critic_loss = temporal_diff_error.pow(2)

        exp_v = torch.zeros(
            temporal_diff_error.size(0), device=logits.device
        )  # Initialize expected value tensor

        a = a.squeeze(1)  # Squeeze out the unnecessary dimension
        logging.debug("Shape of a after squeeze:", a.shape)

        # E: entropy = torch.tensor([0], dtype=torch.float)
        entropy = torch.zeros(1, device=logits.device)  # Initialize entropy
        for dim in range(self.action_dims):  # Iterate over each dimension
            # Compute probabilities for each dimension
            probs = F.softmax(logits[:, dim, :], dim=1)
            # Create a distribution for each dimension
            m = self.distribution(probs)
            # Use a[:, dim] directly without squeezing, ensuring it matches the expected shape
            logging.debug("Shape of a[:, dim]:", a[:, dim].shape)

            log_prob = m.log_prob(a[:, dim])
            # E: entropy -= m.entropy().mean()
            entropy_dim = m.entropy()  # Entropy for each dimension
            entropy += entropy_dim.mean()  # Summing up entropy across dimensions

            # Ensure td is correctly broadcasted during multiplication
            # E: exp_v += (log_prob).sum()
            exp_v += (
                log_prob * temporal_diff_error.detach().squeeze()
            ).sum()  # Expected value

        # Scale by the temporal difference
        # E: exp_v = exp_v * temporal_diff_error.detach().squeeze()
        # Actor loss (policy loss)
        actor_loss = -exp_v.mean()
        # N: entropy_loss = -entropy * entropy_coeff  # Entropy loss
        entropy_loss = -entropy * self.ent_reg  # Entropy loss

        # Total loss is the mean of critic and actor losses
        # E: total_loss = (critic_loss + actor_loss).mean() + self.ent_reg * entropy
        total_loss = (critic_loss + actor_loss + entropy_loss).mean()  # Total loss
        return total_loss

    def loss_func2(self, state, actions, v_t):
        self.train()
        starting_logits, values = self.forward(state)
        logits = self.get_logits_view(starting_logits)
        temporal_diff_error = v_t - values  # Temporal difference error
        critic_loss = temporal_diff_error.pow(2)  # Critic loss (value loss)

        exp_v = torch.zeros(
            temporal_diff_error.size(0), device=logits.device
        )  # Initialize expected value tensor

        actions = actions.squeeze(1)  # Squeeze out the unnecessary dimension
        # if logging_enabled:
        # print("Shape of a after squeeze:", a.shape)

        for dim in range(self.action_dims):  # Iterate over each dimension
            probs = F.softmax(
                logits[:, dim, :], dim=1
            )  # Compute probabilities for each dimension
            m = self.distribution(probs)  # Create a distribution for each dimension
            # Use a[:, dim] directly without squeezing, ensuring it matches the expected shape

            logging.debug("Shape of a[:, dim]:", actions[:, dim].shape)

            log_prob = m.log_prob(actions[:, dim])
            # Ensure temporal_diff_error is correctly broadcasted during multiplication
            exp_v += (log_prob).sum()

        exp_v = (
            exp_v * temporal_diff_error.detach().squeeze()
        )  # Scale by the temporal difference
        a_loss = -exp_v.mean()  # Actor loss (policy loss)

        total_loss = (
            critic_loss + a_loss
        ).mean()  # Total loss is the mean of critic and actor losses
        return total_loss

    def loss_func3(self, s, a, v_t):
        self.train()
        starting_logits, values = self.forward(s)
        logits = self.get_logits_view(starting_logits)
        temporal_diff_error = v_t - values
        critic_loss = temporal_diff_error.pow(2)  # Critic loss (value loss)

        # Initialize expected value tensor
        exp_v = torch.zeros(temporal_diff_error.size(0), device=logits.device)

        a = a.squeeze(1)  # Squeeze out the unnecessary dimension
        logging.info("Shape of a after squeeze:", a.shape)

        for dim in range(self.action_dims):  # Iterate over each dimension
            # Compute probabilities for each dimension
            probs = F.softmax(logits[:, dim, :], dim=1)
            # Create a distribution for each dimension
            m = self.distribution(probs)
            # Multiply the log probabilities by the temporal difference, then sum across all dimensions
            exp_v += (
                m.log_prob(a[:, dim]) * temporal_diff_error.detach().squeeze()
            ).sum()
            # exp_v += (
            #     m.log_prob(a[:, dim]) * temporal_diff_error.detach().squeeze()
            # ).sum()

        # Actor loss (policy loss)
        a_loss = -exp_v.mean()

        # Total loss is the mean of critic and actor losses
        total_loss = (critic_loss + a_loss).mean()
        return total_loss

    def loss_func_old(self, s, a, v_t):
        self.train()
        logits, values = self.forward(s)
        temporal_diff_error = v_t - values
        critic_loss = temporal_diff_error.pow(2)

        probs = F.softmax(logits, dim=1)
        m = self.distribution(probs)
        exp_v = m.log_prob(a) * temporal_diff_error.detach().squeeze()
        actor_loss = -exp_v
        total_loss = (critic_loss + actor_loss).mean()
        return total_loss
