from datetime import datetime
from pathlib import Path
import numpy as np


class ResSaver:
    def __init__(self, params, save_data):
        self.params = params
        self.data = save_data

    def save_data(
        self,
    ):
        # Get the current date and time
        current_datetime = datetime.now().strftime("%Y%m%d-%H%M%S")

        # l = self.params["inner_size"]
        results_dir = Path(".") / "data" / "results" / self.params["env_id"]

        results_dir.mkdir(parents=True, exist_ok=True)

        filename = (
            "res_acrobat_output_cpu"
            + "_".join(f"{k}_{v}" for k, v in self.params.items())
            + "_"
            + current_datetime
            + ".npy"
        )

        np.save(results_dir / filename, self.data)
