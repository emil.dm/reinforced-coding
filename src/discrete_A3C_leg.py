import logging
import os
import time

import hydra
import torch
import torch.multiprocessing as mp
from config import RLconfig
from fig_saver import FigSaver
from hydra.core.config_store import ConfigStore
from mp_logger import logger_process
from networks import Net
from results_saver import ResSaver
from shared_adam import SharedAdam
from utils import set_seeds
from workers import run_leg

# from mp_logger import set_logging
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

cs = ConfigStore.instance()
cs.store(name="leg_rl_config", node=RLconfig)


def run_session(max_workers: int, cfg, logger_name) -> (Net, list):
    logger = logging.getLogger(logger_name)
    logger.propagate = False
    # mp.set_start_method('fork')
    start_time = time.time()
    gnet = Net(
        cfg.envcfg.n_s,
        cfg.envcfg.n_a,
        inner_size=cfg.netcfg.inner_size,
        ent_reg=cfg.traincfg.ent_reg,
        dropout_rate=cfg.traincfg.dropout_rate,
    )  # global network

    gnet.share_memory()  # share the global parameters in multiprocessing
    betas = [float(b) for b in cfg.traincfg.betas.split(", ")]
    opt = SharedAdam(
        # gnet.parameters(), lr=1e-4, betas=(0.92, 0.999)
        gnet.parameters(),
        lr=cfg.traincfg.lr,
        betas=betas,
    )  # global optimizer
    global_ep = mp.Value("i", 0)
    global_ep_r = mp.Value("d", 0.0)
    res_queue = mp.Queue()
    eq_queue = mp.Queue()

    input_rng_seed = cfg.traincfg.global_seed
    # parallel training
    logger.info("Creating workers")
    workers = [
        # LegWorker(
        mp.Process(
            target=run_leg,
            args=(
                cfg.envcfg.n_s,
                cfg.envcfg.n_a,
                gnet,
                opt,
                global_ep,
                global_ep_r,
                res_queue,
                eq_queue,
                logger_name,
                i,
                cfg.envcfg.env_id,
                cfg.traincfg.max_ep,
                cfg.traincfg.update_global_iter,
                cfg.traincfg.gamma,
                cfg.netcfg.inner_size,
                (input_rng_seed + (11 * i)),
                cfg.traincfg.max_steps,
                cfg.traincfg.max_threads,
                cfg.legcfg.time_limit,
                cfg.legcfg.max_episode,
                cfg.traincfg.viewing_enabled,
                cfg.traincfg.max_temp,
                cfg.traincfg.decay_rate,
                cfg.traincfg.async_param,
                cfg.traincfg.chosen_cpu_n,
                cfg.traincfg.dropout_rate,
            ),
        )
        for i in range(max_workers)
    ]

    # Start workers
    for worker in workers:
        worker.start()
        logger.debug(f"Started worker {worker.pid}")
    logger.info("Started all workers")

    avg_reward_history = []  # record episode reward to plot
    reward_history = []  # record episode reward to plot
    while True:
        r = eq_queue.get()  # Retrieves an item from the results queue
        avg_r = res_queue.get()  # Retrieves an item from the results queue

        if r is not None:
            reward_history.append(r)

        if avg_r is not None:
            avg_reward_history.append(avg_r)
        else:
            break

    # join_workers(workers) -> None:
    for worker in workers:
        worker.join()
        logger.info(f"Joined worker {worker.pid}")

    end_time = time.time()

    # Calculate and print the elapsed time
    elapsed_time = end_time - start_time
    logger.info(f"Total execution time: {elapsed_time} seconds")
    return gnet, avg_reward_history, reward_history


@hydra.main(config_path="config", config_name="config", version_base=None)
def main(cfg: RLconfig) -> None:
    os.environ["OMP_NUM_THREADS"] = "1"
    set_seeds(cfg.traincfg.global_seed)

    # create the shared queue
    logging_queue = mp.Queue()
    logging.getLogger().handlers.clear()
    handler = logging.handlers.QueueHandler(logging_queue)

    # Set the formatter for the handler
    formatter = logging.Formatter("[%(asctime)s][%(levelname)s]%(message)s")
    handler.setFormatter(formatter)

    logger_name = "mp-logging"
    logger = logging.getLogger(logger_name)
    logger.propagate = False
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    logger_p = mp.Process(target=logger_process, args=(logging_queue,))
    logger_p.start()
    # set_logging(cfg.traincfg.logging_enabled)

    # Processing arguments is now handled by hydra- the parameter have to be
    # refered with the param section
    # ===-===-
    logger.info(f"Torch version: {torch.__version__}")
    logger.info(f"Working directory : {os.getcwd()}")
    logger.info(
        f"Output directory: {hydra.core.hydra_config.HydraConfig.get().runtime.output_dir}"
    )
    logger.info(f"mp.cpu_count():{mp.cpu_count()}")
    logger.info(f"chosen total CPU : {cfg.traincfg.chosen_cpu_n}")
    max_workers = cfg.traincfg.chosen_cpu_n

    # ===-===-
    all_avg_rewards = []
    all_rewards = []
    all_nets = []
    for r in range(0, cfg.traincfg.total_runs):
        logger.info(f"Currently starting run: {r}")
        gnet, reward_history, eq_history = run_session(max_workers, cfg, logger_name)
        all_nets.append(gnet)
        all_avg_rewards.append(reward_history)
        all_rewards.append(eq_history)

    saving_params = {
        "env_id": cfg.envcfg.env_id,
        "inner_size": cfg.netcfg.inner_size,
        "global_update": cfg.traincfg.update_global_iter,
        "gamma": cfg.traincfg.gamma,
        "MAX_EP": cfg.traincfg.max_ep,
        "max_steps": cfg.traincfg.max_steps,
        "total_runs": cfg.traincfg.total_runs,
        "lr": cfg.traincfg.lr,
        "max_workers": max_workers,
    }

    if cfg.traincfg.dropout_rate != 0.0:
        saving_params["dropout_rate"] = cfg.traincfg.dropout_rate

    if cfg.traincfg.ent_reg != 0:
        saving_params["ent_reg"] = cfg.traincfg.ent_reg

    fig_saver = FigSaver(
        saving_params,
        all_avg_rewards,
        fileprefix="moving_average_et_reward",
        path=cfg.paths.plot,
    )
    fig_saver.save_fig()

    fig_saver2 = FigSaver(
        saving_params,
        all_rewards,
        fileprefix="et_reward",
        path=cfg.paths.plot,
    )
    fig_saver2.save_fig()

    result_saver = ResSaver(saving_params, all_avg_rewards)
    result_saver.save_data()
    logging_queue.put(None)


if __name__ == "__main__":
    mp.set_start_method("spawn")
    main()
