from dm_control import mjcf, viewer, composer
from dm_env import specs

from dm_control.composer.observation import observable
from dm_control.composer import variation
from dm_control.composer.variation import distributions
from dm_control.composer.variation import noises

from dm_control.locomotion.walkers import scaled_actuators

from dm_control.locomotion.soccer import soccer_ball, RandomizedPitch, _area_to_size
from dm_control.locomotion.soccer.team import Team
from dm_control.locomotion.soccer.pitch import MINI_FOOTBALL_GOAL_SIZE
from dm_control.locomotion.soccer.pitch import MINI_FOOTBALL_MAX_AREA_PER_HUMANOID
from dm_control.locomotion.soccer.pitch import MINI_FOOTBALL_MIN_AREA_PER_HUMANOID

# @title Other imports and helper functions

# General
import numpy as np
import os
import collections

random_state = np.random.RandomState(42)

_ASSETS_PATH = os.path.join(
    os.path.dirname(__file__), "../leg-sim/qlearning_sim/robotis_op3"
)

PositionActuatorParams = collections.namedtuple(
    "PositionActuatorParams", ["name", "forcerange", "kp"]
)

_Position_Actuators = [
    PositionActuatorParams("r_hip_yaw", [-18, 18], 18),
    PositionActuatorParams("r_hip_roll", [-18, 18], 18),
    PositionActuatorParams("r_hip_pitch", [-18, 18], 18),
    PositionActuatorParams("r_knee", [-18, 18], 18),
    PositionActuatorParams("r_ank_pitch", [-18, 18], 18),
    PositionActuatorParams("r_ank_roll", [-18, 18], 18),
]


def make_leg():

    xml_path = os.path.join(_ASSETS_PATH, "op3.xml")
    mjcf_model = mjcf_model = mjcf.from_path(xml_path)

    return mjcf_model


class Leg(composer.Entity):
    """A single Robotis op3 leg"""

    def _build(self):
        self._model = make_leg()
        self._model.actuator.motor.clear()
        self._actuators = _Position_Actuators
        for actuator_params in self._actuators:
            associated_joint = self._model.find("joint", actuator_params.name)
            if hasattr(actuator_params, "damping"):
                associated_joint.damping = actuator_params.damping
            actuator = scaled_actuators.add_position_actuator(
                name=actuator_params.name,
                target=associated_joint,
                kp=actuator_params.kp,
                qposrange=associated_joint.range,
                ctrlrange=(-1, 1),
                forcerange=actuator_params.forcerange,
            )
        limits = zip(
            *(actuator.joint.range for actuator in self.actuators)
        )  # pylint: disable=not-an-iterable
        lower, upper = (np.array(limit) for limit in limits)
        self._scale = upper - lower
        self._offset = upper + lower

    def _build_observables(self):
        return LegObservables(self)

    @property
    def action_spec(self):
        minimum, maximum = zip(
            *[
                a.ctrlrange if a.ctrlrange is not None else (-1.0, 1.0)
                for a in self._actuators
            ]
        )
        return specs.BoundedArray(
            shape=(len(self.actuators),),
            dtype=float,
            minimum=minimum,
            maximum=maximum,
            name="\t".join([actuator.name for actuator in self.actuators]),
        )

    def apply_action(self, physics, action, random_state):
        """Apply action to walker's actuators."""
        del random_state
        physics.bind(self.actuators).ctrl = action

    @property
    def mjcf_model(self):
        return self._model

    @property
    def actuators(self):
        return tuple(self._model.find_all("actuator"))


# Add simple observable features for joint angles and velocities.
class LegObservables(composer.Observables):
    @composer.observable
    def joint_positions(self):
        all_joints = self._entity.mjcf_model.find_all("joint")
        return observable.MJCFFeature("qpos", all_joints)

    @composer.observable
    def joint_velocities(self):
        all_joints = self._entity.mjcf_model.find_all("joint")
        return observable.MJCFFeature("qvel", all_joints)


NUM_SUBSTEPS = 25  # The number of physics substeps per control timestep.

# @title The `PressWithSpecificForce` task


class KickBall(composer.Task):
    def __init__(self, leg, arg_random_state):
        self._leg = leg
        self.random_state = arg_random_state
        self._arena = RandomizedPitch(
            min_size=_area_to_size(MINI_FOOTBALL_MIN_AREA_PER_HUMANOID * 1),
            max_size=_area_to_size(MINI_FOOTBALL_MAX_AREA_PER_HUMANOID * 1),
            keep_aspect_ratio=True,
            field_box=False,
            goal_size=MINI_FOOTBALL_GOAL_SIZE,
            top_camera_distance=5,
        )
        self._arena.mjcf_model.worldbody.add("light", pos=(0, 0, 4))
        self._arena.attach(self._leg)
        # Create ball and attach ball to arena.
        self._ball = soccer_ball.SoccerBall(
            radius=0.081, mass=0.025, friction=(0.7, 0.05, 0.04), damp_ratio=0.4
        )
        self._arena.add_free_entity(self._ball)
        self._arena.register_ball(self._ball)

        # Configure initial poses
        self._leg_initial_pose = (-0.15, 0.05, 0.01)
        # 7.532 , 0 , 0 to goal line
        self._ball_initial_pose = (0, 0, 0)

        # Configure variators
        self._mjcf_variator = variation.MJCFVariator()
        self._physics_variator = variation.PhysicsVariator()

        # Configure and enable observables
        pos_corrptor = noises.Additive(distributions.Normal(scale=0.01))
        self._leg.observables.joint_positions.corruptor = pos_corrptor
        self._leg.observables.joint_positions.enabled = True
        vel_corruptor = noises.Multiplicative(distributions.LogNormal(sigma=0.01))
        self._leg.observables.joint_velocities.corruptor = vel_corruptor
        self._leg.observables.joint_velocities.enabled = True

        self.control_timestep = NUM_SUBSTEPS * self.physics_timestep

        def _tracked_entity_positions(physics):
            """Return a list of the positions of the ball and all players."""
            ball_pos, unused_ball_quat = self._ball.get_pose(physics)
            entity_positions = [ball_pos]
            return entity_positions

        self._task_observables = {}
        self._task_observables["ball_position"] = observable.Generic(
            _tracked_entity_positions
        )

        for obs in self._task_observables.values():
            obs.enabled = True

    @property
    def root_entity(self):
        return self._arena

    @property
    def task_observables(self):
        return self._task_observables

    def initialize_episode_mjcf(self, random_state):
        self._arena.initialize_episode_mjcf(random_state)

    def initialize_episode(self, physics, random_state):
        self._arena.initialize_episode(physics, random_state)
        leg_pose = variation.evaluate(
            (self._leg_initial_pose), random_state=self.random_state
        )
        ball_pose = variation.evaluate(
            (self._ball_initial_pose), random_state=self.random_state
        )
        self._leg.set_pose(physics, position=leg_pose)
        self._ball.set_pose(physics, position=ball_pose)

    def _tracked_entity_positions(self, physics):
        """Return a list of the positions of the ball and all players."""
        ball_pos, unused_ball_quat = self._ball.get_pose(physics)
        entity_positions = [ball_pos]
        return entity_positions

    def get_reward(self, physics):
        # print("Team.HOME: ", Team.HOME)
        goal_scored = self._arena.detected_goal()
        # print("goal_scored: ", goal_scored)
        if not goal_scored == None and goal_scored == Team.HOME:
            return 1
        ball_pos = self._tracked_entity_positions(physics)
        # print("ball_pos[0][0]: ",ball_pos[0][0])
        # print("ball_pos: ",ball_pos)
        return np.clip(ball_pos[0][0] / 7.532, 0, 1)

    # def should_terminate_episode(self, physics):
    #   """Returns True if a goal was scored by either team."""
    #   return self._arena.detected_goal() is not None

    def before_step(self, physics, action, random_state):
        self._leg.apply_action(physics, action, random_state)


# @title Instantiating an environment{vertical-output: true}


class leg_env:
    def __init__(self, time_limit, max_episode, arg_random_state):
        leg = Leg()
        task = KickBall(leg, arg_random_state)
        self.env = composer.Environment(
            task, random_state=arg_random_state, time_limit=time_limit
        )
        self.max_episode = max_episode
        self.action_spec = self.env.action_spec()
