import logging
import os
import random

import gymnasium as gym
import numpy as np
import torch
import torch.multiprocessing as mp
from dm_control import viewer
from leg_policy import LegPolicy
from networks import Net
from qlearning_sim.leg_env import leg_env
from utils import push_and_pull
from utils import update_and_record
from utils import v_wrap

def run_leg(
    N_S,
    N_A,
    gnet,
    opt,
    global_ep,
    global_ep_r,
    res_queue,
    eq_queue,
    logger_name,
    name,
    env_id="leg",
    max_ep=500000,
    update_global_iter=0,
    gamma=0.9,
    inner_size=250,
    input_rng_seed=111777,
    max_steps=40,
    max_threads=1,
    time_limit=500,
    max_episode=1500,
    viewer_enabled=False,
    max_temp=1,
    decay_rate=0.999,
    async_param=0,
    chosen_cpu_n=1,
    dropout_rate=0.0,
):
    # create a logger
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)
    process = mp.current_process()

    logger.info(f"Running leg worker on process {process.name}")
    logger.debug("Set threads")
    torch.set_num_threads(max_threads)
    pid = os.getpid()
    logger.debug(f"[Worker {pid}] Starting process")

    lnet = Net(N_S, N_A, inner_size=inner_size, dropout_rate=dropout_rate)  # local network
    action_bins = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0], dtype=float)
    action_bins[0] = -0.3
    for nn in range(1, 6):
        action_bins[nn] = action_bins[nn - 1] + 0.1

    try:
        # Q Shouldn't this random stae be different for every worker?
        # NN 5s = 100 steps; 10s = 200 steps
        env0 = leg_env(
            time_limit,
            max_episode,
        )

        total_step = 1

        if viewer_enabled:
            # viewer.launch(env0.env, policy=policy.run)
            pass

        else:
            logger.info("hello2")
            epi_count = 1
            while global_ep.value < max_ep:
                buffer_s, buffer_a, buffer_r = [], [], []
                ep_r = 0.0

                timestep = env0.env.reset()
                s = timestep.observation
                env0.env.task.ball_stop = False

                # Flatten and concatenate the arrays
                combined_s = np.concatenate(
                    [
                        s["ball_position"].flatten(),
                        s["robotis_op3/joint_positions"].flatten(),
                        s["robotis_op3/joint_velocities"].flatten(),
                    ]
                )

                combined_s = combined_s[None, :]

                logger.debug("Reset state:", combined_s)

                lnet.adjust_temperature(epi_count, max_temp, decay_rate)
                # E: worker_max_steps = max_steps
                # Q: Why this here?
                additional_steps = random.randint(0, async_param * chosen_cpu_n)
                worker_max_steps = max_steps + additional_steps
                done = False

                for t in range(worker_max_steps):
                    logger.debug(
                        f"[Worker {pid}] Step {t} in episode {global_ep.value}"
                    )
                    # a = lnet.choose_action(v_wrap(s[None, :]))
                    a = lnet.choose_action2(
                        v_wrap(combined_s),
                    )
                    action = np.array(
                        [
                            action_bins[a[0, 0]],
                            action_bins[a[0, 1]],
                            action_bins[a[0, 2]],
                            action_bins[a[0, 3]],
                            action_bins[a[0, 4]],
                            action_bins[a[0, 5]],
                        ],
                        dtype=float,
                    )

                    timestep = env0.env.step(action)

                    s_ = timestep.observation
                    r = timestep.reward

                    if timestep.last():
                        done = True
                        logger.info("SIMULATOR STEPS TIME UP!!!!  t = ", t)

                    combined_s_ = np.concatenate(
                        [
                            s_["ball_position"].flatten(),
                            s_["robotis_op3/joint_positions"].flatten(),
                            s_["robotis_op3/joint_velocities"].flatten(),
                        ]
                    )[None, :]

                    if t == (max_steps - 1):
                        done = True

                    if env0.env.task.ball_stop:
                        done = True

                    logger.debug(
                        "Player {}: reward = {}, done = {}, observations = {}.".format(
                            pid, r, done, combined_s_
                        )
                    )

                    ep_r += r
                    buffer_s.append(combined_s)
                    buffer_a.append(a)
                    buffer_r.append(r)

                    # worker_UPDATE_GLOBAL_ITER = (
                    #     UPDATE_GLOBAL_ITER + additional_steps
                    # )
                    # :E if (
                    # :E     total_step % update_global_iter == 0 or done
                    # :E  ):  # update global and assign to local net
                    # update global and assign to local net
                    if done:
                        # sync
                        push_and_pull(
                            opt,
                            lnet,
                            gnet,
                            done,
                            combined_s_,
                            buffer_s,
                            buffer_a,
                            buffer_r,
                            gamma,
                        )
                        buffer_s, buffer_a, buffer_r = [], [], []

                        if done:  # done and print information
                            update_and_record(
                                global_ep,
                                global_ep_r,
                                ep_r,
                                lnet.T,
                                res_queue,
                                eq_queue,
                                name,
                            )
                            log_msg = f"{name:3}, Ep: {global_ep.value:5} | Glo_Ep_r: {global_ep_r.value:2.4f} | Ep_r: {ep_r:2.4f} | ep_T: {lnet.T:1.5f}"
                            logger.info(log_msg)
                            break
                    s = s_
                    combined_s = combined_s_
                    total_step += 1

                logger.debug("loc_ep_r: ", ep_r)
                epi_count = epi_count + 1
        res_queue.put(None)
        eq_queue.put(None)
        logger.debug(f"[Worker {pid}] Process completed")
    except Exception as e:
        logger.info("Error in worker...")
        logger.error(f"[Worker {pid}] Exception occurred", exc_info=True)
        raise (e)
