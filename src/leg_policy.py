import numpy as np
import logging


class LegPolicy:
    # This is the init of the policy and creates our action space
    def __init__(self):
        self.action = np.array([0, 0, 0, 0, 0, 0], dtype=float)

    # The run function is what the env will call, this function needs to
    # return the action space, it needs to take self and time_step
    def run(self, timestep):
        # time_step contains reward, discount and observations
        logging.info("self.action in policy: ", self.action)
        return self.action
