from dataclasses import dataclass


@dataclass
class Paths:
    log: str
    plot: str
    data: str
    results: str


@dataclass
class EnvCfg:
    env_id: str
    n_s: int
    n_a: int


@dataclass
class LegCfg:
    # Leg env
    time_limit: 500
    max_episode: 500000


@dataclass
class NetCfg:
    inner_size: int


@dataclass
class TrainCfg:
    logging_enabled: bool
    viewing_enabled: bool
    update_global_iter: int
    gamma: float
    max_ep: int
    global_seed: int
    max_steps: int
    chosen_cpu_n: int
    max_threads: int
    total_runs: int
    lr: float
    betas: tuple
    ent_reg: float
    max_temp: float
    decay_rate: float
    async_param: float


@dataclass
class RLconfig:
    paths: Paths
    envcfg: EnvCfg
    legcfg: LegCfg
    netcfg: NetCfg
    traincfg: TrainCfg
