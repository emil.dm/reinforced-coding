import matplotlib.pyplot as plt
from datetime import datetime
from pathlib import Path
import numpy as np
from numpy.lib.stride_tricks import sliding_window_view


class FigSaver:
    def __init__(self, params, plt_data, fileprefix, path=".", window_size=5):
        self.params = params
        self.plt_data = plt_data
        self.fileprefix = fileprefix
        self.savepath = path
        self.window_size = window_size

    def save_fig(
        self,
    ):
        # Get the current date and time
        current_datetime = datetime.now().strftime("%Y%m%d-%H%M%S")

        layer_size = self.params["inner_size"]
        plt_dir = (
            Path(self.savepath) / self.params["env_id"] / f"layer_size{layer_size }"
        )

        plt_dir.mkdir(parents=True, exist_ok=True)

        full_filename = (
            self.fileprefix
            + "_".join(f"{k}_{v}" for k, v in self.params.items())
            + "_"
            + current_datetime
            + ".png"
        )

        if len(self.plt_data) < 2:
            self.plot_trace()
        else:
            # self.plot_from_multiple_runs()
            self.plot_moving_average()

        print("Saving file at path", plt_dir.resolve())
        plt.savefig(plt_dir / full_filename)

    def plot_trace(self,):
        plt.plot(self.plt_data)
        plt.ylabel("Moving average ep reward")
        plt.xlabel("Episode")

    def plot_from_multiple_runs(self,):
        plt.figure()
        total_items = len(self.plt_data[0])
        # Create the plot
        data_mean = np.mean(self.plt_data, axis=0)
        x = np.arange(total_items)  # Example x-axis data

        # Plot the mean
        plt.plot(x, data_mean, label="Mean", color="blue")
        # markers, caps, bars = plt.errorbar(x, mean, yerr=std_dev, fmt='r', label='Standard Deviation')
        for count, d in enumerate(self.plt_data):
            if count == 0:
                label = "data"
            else:
                label = ""
            plt.scatter(x, d, label=label, color="red", alpha=0.6)
        plt.ylabel("Moving average ep reward")
        plt.xlabel("Episode")
        plt.legend()

    def plot_moving_average(self,):
        plt.figure(figsize=(12, 8))
        total_items = len(self.plt_data[0])
        # Create the plot
        data_mean = np.mean(self.plt_data, axis=0)
        rolling_avg_loc_res = sliding_window_view(data_mean, self.window_size).mean(axis=-1)
        x = np.arange(total_items)  # Example x-axis data
        x_shifted = np.arange(len(rolling_avg_loc_res))+self.window_size-1  # Example x-axis data

        # Plot the mean
        plt.plot(x, data_mean, label="Mean", color="blue")
        plt.plot(x_shifted, rolling_avg_loc_res, label='sliding_ep_r', color='tab:green', )

        for count, d in enumerate(self.plt_data):
            if count == 0:
                label = "data"
            else:
                label = ""
            plt.scatter(x, d, label=label, color="red", alpha=0.6)
        plt.ylabel('(Average) Episode Return')
        plt.xlabel("Episode")
        plt.legend()